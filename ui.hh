/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef UI_H
#define UI_H

#include <QMainWindow>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <QTextLayout>

#include "session.hh"

/* This is the thing that deals with all QT objects */
class UI {
        public:
                explicit UI(Session *session, QWidget *parent = 0);
                UI(const UI& that) = delete;
                UI(const UI&& that) = delete;
                ~UI();

                UI& operator=(const UI& that) = delete;
                UI& operator=(const UI&& that) = delete;

                void show();

        private:
                /* Reset text/styling according to what's currently going on in the session */
                void sync_with_session();

                /* QT stuff; all added to q_main_window and deleted from it */
                QMainWindow *q_main_window;
                QAction *action_exit;
                QWidget *central_widget;

                /* Layouts */
                QVBoxLayout *box_layout_0;
                QHBoxLayout *box_layout_top;
                QHBoxLayout *box_layout_bottom;
                QVBoxLayout *box_layout_top_left;
                QVBoxLayout *box_layout_top_main;

                /* Main display */
                QLabel *status_previously_seen;
                QScrollArea *scrollable_main_card;
                QLabel *main_card;

                /* Knowing card */
                QLabel *remembered;
                QPushButton *button_easy;
                QPushButton *button_hard;
                QPushButton *button_fail;

                /* Flipping */
                QPushButton *button_flip;

                /* Bottom */
                QPushButton *button_prev;
                QLabel *status_progress;
                QPushButton *button_next;

                /*  Menus */
                QMenuBar *menu_bar;
                QMenu *menu_bar_file;

                /* State stuff; owned by main so we don't need to worry about it. */
                Session *session;
};

#endif
