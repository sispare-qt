/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef CARD_H
#define CARD_H

#include <filesystem>
#include <variant>

/* What's gone on for a particular card */
enum class Review_status { Unreviewed, Pass_easy, Pass_hard, Fail };

/*
 * This is an individual flash card. It has a front side (A), a reverse
 * side (B), and some information about what level of review it's
 * currently at.
 *
 * The hash thing is just so that we can trivially shuffle these cards
 * for review by sticking them into an ordered map. We salt with a
 * per-run value Session::salt.
 */
class Card {
        public:
                Card() = delete;

                bool operator==(const Card& that) const;
                std::weak_ordering operator<=>(const Card& that) const;

                static Card mk(const std::filesystem::path&dir);

                /* For debugging */
                friend std::ostream& operator<< (std::ostream &out, const Card &us);

                /* The directory defining this card */
                const std::filesystem::path path;

                /* The contents of this card's A-side */
                const std::string a;

                /* The contents of this card's B-side */
                const std::string b;

                /* The hash used for (shuffled) comparison */
                const std::size_t hash;

                /* The mastery level of this card */
                const int level;

        private:
                explicit Card(const std::filesystem::path& path, const std::string& a, const std::string& b, int level);

};

#endif
