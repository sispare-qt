.SUFFIXES:
.SUFFIXES: .o .cc

CFLAGS ?=
LDFLAGS ?=
PKG_CONFIG ?= pkg-config

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin

# MOC ?= /usr/bin/moc
# UIC ?= /usr/bin/uic

CXXFLAGS += -std=c++20 -D_DEFAULT_SOURCE

# Qt5 stuff
CXXFLAGS += -fPIC # Why isn't this in --cflags ?
CXXFLAGS += $(shell $(PKG_CONFIG) --cflags Qt5Core Qt5Gui Qt5Widgets)
LDFLAGS +=  $(shell $(PKG_CONFIG) --libs   Qt5Core Qt5Gui Qt5Widgets)

# Debug
# CXXFLAGS += -ggdb -O0
CXXFLAGS += -pedantic -Wall -Wextra -Werror

default: all
all: \
	sispare-qt

%.o: %.cc $(wildcard *.hh)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

sispare-qt: main.o card.o session.o ui.o util.o
	$(CXX) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	find -name '*.o' -delete
	rm -f sispare-qt

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f sispare-qt $(DESTDIR)$(BINDIR)/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f sispare-qt
