/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "ui.hh"

#include <QCloseEvent>
#include <QSettings>
#include <QSpacerItem>
#include <QtWidgets/QScrollBar>

/*
 * Thin wrapper around QMainWindow with just enough logic to save window
 * settings.
 */
class ThisMainWindow : public QMainWindow {
        public:
                ThisMainWindow(Session *the_session, QWidget *parent) :
                        QMainWindow(parent)
                {
                        session = the_session;
                        settings_read();
                }

                void
                closeEvent(QCloseEvent *event) override
                {
                        settings_write();
                        session->update_cards_on_disk();
                        event->accept();
                }

                void
                settings_read()
                {
                        QSettings settings;

                        settings.beginGroup("MainWindow");
                        resize(settings.value("size", QSize(400, 300)).toSize());
                        move(settings.value("pos", QPoint(200, 200)).toPoint());
                        settings.endGroup();
                }

                void
                settings_write() const
                {
                        QSettings settings;

                        settings.beginGroup("MainWindow");
                        settings.setValue("size", size());
                        settings.setValue("pos", pos());
                        settings.endGroup();
                }

        private:
                Session *session;
};

/*
 * Slightly modified QScrollArea to only allow vertical scrolling, and
 * Mostly from https://forum.qt.io/topic/13374/solved-qscrollarea-vertical-scroll-only
 * .
 */
class QVerticalScrollArea : public QScrollArea {
        public:
                QVerticalScrollArea(QWidget *parent = 0) :
                        QScrollArea(parent)
                {
                        setFrameStyle(QFrame::NoFrame);
                        setWidgetResizable(true);
                        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
                        setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
                }

                bool
                eventFilter(QObject *o, QEvent *e)
                {
                        if (o &&
                            o == widget() &&
                            e->type() == QEvent::Resize) {
                                setMinimumWidth(widget()->minimumSizeHint().width() + verticalScrollBar()->width());
                        }

                        return QScrollArea::eventFilter(o, e);
                }

};

UI::UI(Session *the_session, QWidget *parent)
{
        session = the_session;

        /* Set up QT stuff */
        q_main_window = new ThisMainWindow(session, parent);

        if (q_main_window->objectName().isEmpty()) {
                q_main_window->setObjectName(QString::fromUtf8("sispare-qt"));
        }

        q_main_window->setWindowTitle(QString::fromUtf8("Sispare"));

        /* Exiting */
        action_exit = new QAction("E&xit", q_main_window);
        action_exit->setObjectName(QString::fromUtf8("action_exit"));
        QObject::connect(action_exit, &QAction::triggered, [this](){
                q_main_window->close();
        });

        /* Layout */

        /*
         +--------------------------------------------------+
         |  B1   |                T3                        |
         |-------|------------------------------------------|
         |  B2   |                                          |
         |-------|               T1                         |
         |  B3   |                                          |
         |-------|                                          |
         |-------|                                          |
         |  BF   |                                          |
         |--------------------------------------------------|
         |  B<      |          T2               |    B>     |
         +--------------------------------------------------+

           T1: Display of the A/B side for current card
           B1-3: Easy/Hard/Fail buttons
           BF: Flip A/B
           B</<: Forward/Back buttons
           T2: Current progress indicator
           T3: If we've already marked this card as something
         */
        central_widget = new QWidget(q_main_window);
        central_widget->setObjectName(QString::fromUtf8("central_widget"));

        /* Holds everything */
        box_layout_0 = new QVBoxLayout();
        box_layout_0->setObjectName(QString::fromUtf8("box_layout_0"));
        box_layout_0->setSizeConstraint(QLayout::SetMinimumSize);

        /* Holds T1, T3, B1-3, and BF */
        box_layout_top = new QHBoxLayout();
        box_layout_top->setObjectName(QString::fromUtf8("box_layout_top"));
        box_layout_top->setSizeConstraint(QLayout::SetMinimumSize);

        /* Holds B<, T2, B> */
        box_layout_bottom = new QHBoxLayout();
        box_layout_bottom->setObjectName(QString::fromUtf8("box_layout_bottom"));

        /* Holds B1-3 and BF */
        box_layout_top_left = new QVBoxLayout();
        box_layout_top_left->setObjectName(QString::fromUtf8("box_layout_top_left"));

        /* Holds T3 and T1 */
        box_layout_top_main = new QVBoxLayout();
        box_layout_top_main->setObjectName(QString::fromUtf8("box_layout_top_main"));
        box_layout_top_main->setSizeConstraint(QLayout::SetMinimumSize);

        /*  T3 */
        status_previously_seen = new QLabel();
        status_previously_seen->setObjectName(QString::fromUtf8("status_previously_seen"));
        status_previously_seen->setAlignment(Qt::AlignHCenter);
        status_previously_seen->setText(QString::fromUtf8("(already marked as FAILED)"));

        /* T1 */
        scrollable_main_card = new QVerticalScrollArea();
        scrollable_main_card->setObjectName(QString::fromUtf8("scrollable_main_card"));
        scrollable_main_card->setAlignment(Qt::AlignHCenter);
        main_card = new QLabel();
        main_card->setObjectName(QString::fromUtf8("main_card"));
        main_card->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        main_card->setWordWrap(true);
        main_card->setTextInteractionFlags(Qt::TextSelectableByMouse);
        main_card->setAlignment(Qt::AlignCenter);

        /* B1-3 caption */
        remembered = new QLabel();
        remembered->setObjectName(QString::fromUtf8("remembered"));
        remembered->setText(QString::fromUtf8("Remembered?"));

        /* B1 */
        button_easy = new QPushButton();
        button_easy->setObjectName(QString::fromUtf8("button_easy"));
        button_easy->setText(QString::fromUtf8("&Yes"));
        QObject::connect(button_easy, &QPushButton::clicked, [this](){
                session->mark_current_card_as(Review_status::Pass_easy);
                sync_with_session();
        });

        /* B2 */
        button_hard = new QPushButton();
        button_hard->setObjectName(QString::fromUtf8("button_hard"));
        button_hard->setText(QString::fromUtf8("&Hard"));
        QObject::connect(button_hard, &QPushButton::clicked, [this](){
                session->mark_current_card_as(Review_status::Pass_hard);
                sync_with_session();
        });

        /* B3 */
        button_fail = new QPushButton();
        button_fail->setObjectName(QString::fromUtf8("button_fail"));
        button_fail->setText(QString::fromUtf8("&No"));
        QObject::connect(button_fail, &QPushButton::clicked, [this](){
                session->mark_current_card_as(Review_status::Fail);
                sync_with_session();
        });

        /* BF */
        button_flip = new QPushButton();
        button_flip->setObjectName(QString::fromUtf8("button_flip"));
        button_flip->setText(QString::fromUtf8("Flip &Over"));
        QObject::connect(button_flip, &QPushButton::clicked, [this](){
                session->flip_current_card();
                sync_with_session();
        });

        /* B< */
        button_prev = new QPushButton();
        button_prev->setObjectName(QString::fromUtf8("button_prev"));
        button_prev->setText(QString::fromUtf8("Back"));
        QObject::connect(button_prev, &QPushButton::clicked, [this](){
                session->move_prev_card();
                sync_with_session();
        });

        /* B> */
        button_next = new QPushButton();
        button_next->setObjectName(QString::fromUtf8("button_next"));
        button_next->setText(QString::fromUtf8("Next"));
        QObject::connect(button_next, &QPushButton::clicked, [this](){
                session->move_next_card();
                sync_with_session();
        });

        /* T2 */
        status_progress = new QLabel();
        status_progress->setObjectName(QString::fromUtf8("status_progress"));

        /* Add everything to everything else */
        box_layout_top_left->addSpacing(75);
        box_layout_top_left->addWidget(remembered);
        box_layout_top_left->addSpacing(25);
        box_layout_top_left->addWidget(button_easy);
        box_layout_top_left->addWidget(button_hard);
        box_layout_top_left->addWidget(button_fail);
        box_layout_top_left->addStretch(0);
        box_layout_top_left->addWidget(button_flip);
        box_layout_top_left->addStretch(0);
        scrollable_main_card->setWidget(main_card);
        box_layout_top_main->addWidget(status_previously_seen);
        box_layout_top_main->addWidget(scrollable_main_card);
        box_layout_bottom->addSpacing(100);
        box_layout_bottom->addWidget(button_prev);
        box_layout_bottom->addStretch(0);
        box_layout_bottom->addWidget(status_progress);
        box_layout_bottom->addStretch(0);
        box_layout_bottom->addWidget(button_next);
        box_layout_bottom->addSpacing(100);
        box_layout_top->addLayout(box_layout_top_left);
        box_layout_top->addSpacing(15);
        box_layout_top->addLayout(box_layout_top_main);
        box_layout_0->addLayout(box_layout_top);
        box_layout_0->addLayout(box_layout_bottom);
        central_widget->setLayout(box_layout_0);
        q_main_window->setCentralWidget(central_widget);

        /* Menu bar stuff */
        menu_bar = new QMenuBar(q_main_window);
        menu_bar->setObjectName(QString::fromUtf8("menu_bar"));
        menu_bar->setGeometry(QRect(0, 0, 243, 21));
        menu_bar_file = new QMenu(menu_bar);
        menu_bar_file->setTitle(QString::fromUtf8("&File"));
        menu_bar_file->setObjectName(QString::fromUtf8("menu_bar_file"));
        q_main_window->setMenuBar(menu_bar);
        menu_bar->addAction(menu_bar_file->menuAction());
        menu_bar_file->addAction(action_exit);

        /* Now the UI is all set up. Start the session by showing the first card */
        sync_with_session();
}

UI::~UI()
{
        /* Deletes all children as well */
        delete q_main_window;
}

void
UI::show()
{
        q_main_window->show();
}

/*
 * Deal with the pretty simple state exposed by Session. Enable/disable
 * buttons and set text contents as appropriate.
 */
void
UI::sync_with_session()
{
        status_previously_seen->setText(QString::fromStdString(session->get_previously_seen_string()));
        button_prev->setEnabled(session->have_prev_card());
        button_next->setEnabled(session->have_next_card());
        status_progress->setText(QString::fromStdString(session->get_progress_string()));

        switch (session->get_current_action()) {
        case Currently_doing::Viewing_A:
                main_card->setText(QString::fromStdString(session->get_current_A_side().value_or("")));
                main_card->setAlignment(Qt::AlignCenter);
                main_card->setStyleSheet("#main_card { font-size: 80pt; }");
                status_progress->setVisible(true);
                button_easy->setEnabled(false);
                button_hard->setEnabled(false);
                button_fail->setEnabled(false);
                button_flip->setEnabled(true);
                break;
        case Currently_doing::Viewing_B:
                main_card->setText(QString::fromStdString(session->get_current_B_side().value_or("")));
                main_card->setAlignment(Qt::AlignJustify);
                main_card->setStyleSheet("#main_card { font-size: 14pt; }");
                status_progress->setVisible(true);
                button_easy->setEnabled(true);
                button_hard->setEnabled(true);
                button_fail->setEnabled(true);
                button_flip->setEnabled(true);
                break;
        case Currently_doing::Finished:
                main_card->setText(QString::fromStdString(session->get_statistics()));
                main_card->setAlignment(Qt::AlignHCenter);
                main_card->setStyleSheet("#main_card { }");
                status_progress->setVisible(false);
                button_easy->setEnabled(false);
                button_hard->setEnabled(false);
                button_fail->setEnabled(false);
                button_flip->setEnabled(false);
                break;
        case Currently_doing::Trivial:
                main_card->setText(QString::fromUtf8("No review necessary."));
                main_card->setAlignment(Qt::AlignHCenter);
                main_card->setStyleSheet("#main_card { }");
                status_progress->setVisible(false);
                button_easy->setEnabled(false);
                button_hard->setEnabled(false);
                button_fail->setEnabled(false);
                button_flip->setEnabled(false);
                break;
        }
}
