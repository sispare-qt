/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef SESSION_H
#define SESSION_H

#include <filesystem>
#include <optional>
#include <map>
#include <set>
#include <variant>
#include <vector>

#include "card.hh"

/* What's currently going on */
enum class Currently_doing { Viewing_A, Viewing_B, Finished, Trivial };

/*
 * This holds the abstract information about a review session: a list of
 * cards scheduled for review and what action has been taken on each card.
 *
 * Thankfully rule-of-zero-compliant.
 */
class Session {
        public:
                Session(const std::filesystem::path& data_dir);

                /* A changing (but not strongly random) value per-run. Used by Card in shuffled ordering */
                static std::string salt;

                /*
                 * A card at level j takes between timing_data[j].first
                 * and timing_data[j].second days to review next. Not
                 * computed because I might want to fiddle with it.
                 */
                static constexpr std::pair<int, int> timing_data[13] = {
                        /* */
                        std::make_pair(0, 0),     /* */
                        std::make_pair(1, 1),     /* */
                        std::make_pair(1, 2),     /* */
                        std::make_pair(2, 3),     /* */
                        std::make_pair(3, 5),     /* */
                        std::make_pair(5, 8),     /* */
                        std::make_pair(8, 13),    /* */
                        std::make_pair(13, 21),   /* */
                        std::make_pair(21, 34),   /* */
                        std::make_pair(34, 55),   /* */
                        std::make_pair(55, 89),   /* */
                        std::make_pair(89, 144),  /* */
                        std::make_pair(144, 233), /* */
                };

                /* Are the any cards to review in this session? */
                bool have_cards_to_review() const;

                /* What's going on right now? */
                Currently_doing get_current_action() const;

                /* Get the A-side of the card that's being looked at */
                std::optional<const std::string> get_current_A_side() const;

                /* Get the B-side of the card that's being looked at */
                std::optional<const std::string> get_current_B_side() const;

                /* Get something like "19/23" */
                const std::string get_progress_string() const;

                /* Get something like "Finished. 10 passed, 3 failed" */
                const std::string get_statistics() const;

                /* Get something like "Already marked as passed" */
                const std::string get_previously_seen_string() const;

                /* Are there cards after this one? */
                bool have_next_card() const;

                /* Are there cards before this one? */
                bool have_prev_card() const;

                /* Flip the current card over */
                void flip_current_card();

                /* Mark the current card as having had some review action taken */
                void mark_current_card_as(Review_status review_status);

                /* Move to the next card */
                void move_next_card();

                /* Move to the previous card */
                void move_prev_card();

                /* Write out new levels and schedule files according to this session's progress */
                void update_cards_on_disk();

        private:
                /* Something like "~/.data/sispare" */
                const std::filesystem::path data_dir;

                /* When this session was started */
                std::chrono::system_clock::time_point session_start;

                /* What the UI should show right now */
                Currently_doing current_action;

                /* All the cards that are due for review this session. */
                std::set<Card> cards;

                /* Our iterator into cards */
                std::set<Card>::iterator cards_it;

                /* ... and the actual index */
                std::size_t it_index;

                /* What happened to each card? Indexed by path for use by update_cards_on_disk. */
                std::map<std::filesystem::path, Review_status> card_status;

                /* The schedule files that may need to be updated if we review some cards */
                std::vector<std::filesystem::path> schedule_files_to_clean;

                /* The cards which we failed reading; therefore cards we should delete */
                std::set<std::filesystem::path> cards_to_delete;
};

#endif
