/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "util.hh"

#include <fstream>
#include <iostream>

/* Find the right "~/.data/sispare" or "~/.sispare" or something */
std::filesystem::path
Util::find_data_dir()
{
        char *xdg_data_home = getenv("XDG_DATA_HOME");

        if (xdg_data_home) {
                return std::filesystem::path(xdg_data_home) / "sispare";
        }

        char *home = getenv("HOME");

        if (home) {
                return std::filesystem::path(home) / ".sispare";
        }

        throw std::runtime_error("You have no $HOME. I give up.");
}

/* Print exception, maybe with stack trace */
void
Util::output_nested_ex(const std::exception& ex, int level)
{
        if (level == 0) {
                std::cerr << "Error:" << std::endl;
        }

        std::cerr << std::string(level, '.') << ex.what() << std::endl;
        try {
                std::rethrow_if_nested(ex);
        } catch (const std::exception& ex2) {
                output_nested_ex(ex2, level + 1);
        } catch (...) {}
}

/* Slurp file into string (for reading A/B sides). throws on failure */
std::string
Util::slurp(const std::filesystem::path& path)
{
        std::ifstream ii(path, std::ios::in | std::ios::binary);

        if (ii) {
                std::string slurped;

                ii.seekg(0, std::ios::end);
                slurped.resize(ii.tellg());
                ii.seekg(0, std::ios::beg);
                ii.read(&slurped[0], slurped.size());
                ii.close();

                /* Remove trailing newline */
                if (std::isspace(slurped.back())) {
                        slurped.pop_back();
                }

                return slurped;
        }

        std::ostringstream err;

        err << "Cannot read " << path;
        throw std::runtime_error(err.str());
}
