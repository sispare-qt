/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "card.hh"

#include <fstream>
#include <string>

#include "session.hh"
#include "util.hh"

/* Constructor. */
Card::Card(const std::filesystem::path& path, const std::string& a, const std::string& b, int level) :
        path(path),
        a(a),
        b(b),
        hash(std::hash<std::string>{}(Session::salt + path.string())), level(level)
{
}

/* Cards should be completely determined by the full path to their base dir */
bool
Card::operator==(const Card& that) const
{
        return this->path == that.path;
}

/* We actually want these cards shuffled */
std::weak_ordering
Card::operator<=>(const Card& that) const
{
        return this->hash <=> that.hash;
}

/* Builder method, throwing errors if any of the files are unreadable/malformed */
Card
Card::mk(const std::filesystem::path& dir)
{
        try {
                std::string a_str = Util::slurp(dir / "side_A");
                std::string b_str = Util::slurp(dir / "side_B");
                std::string level_str = Util::slurp(dir / "level");
                int level_int = 0;

                try {
                        level_int = std::stoi(level_str);
                } catch (const std::exception& e) {
                        std::ostringstream err;

                        err << "Invalid level `" << level_str << "' for card " << dir.stem();
                        std::throw_with_nested(std::runtime_error(err.str()));
                }

                return Card(dir, a_str, b_str, level_int);
        } catch (...) {
                std::ostringstream err;

                err << "Cannot parse card " << dir.stem();
                std::throw_with_nested(std::runtime_error(err.str()));
        }
}

/* For debugging */
std::ostream&
operator<< (std::ostream &out, const Card& us)
{
        return out << us.path;
}
