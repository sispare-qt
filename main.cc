/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <cstdlib>
#include <iostream>
#include <QApplication>

#include <unistd.h>

#include "session.hh"
#include "ui.hh"
#include "util.hh"

int
main(int argc, char *argv[])
{
        try {
                /* The only option is [ -d <directory> ] */
                int c;
                char *explicit_data_dir_str = nullptr;

                while ((c = getopt(argc, argv, "d:")) != -1) {
                        switch (c) {
                        case 'd':

                                if (optarg) {
                                        explicit_data_dir_str = optarg;
                                }

                                break;
                        }
                }

                /* Read in the cards */
                std::filesystem::path data_dir;

                if (explicit_data_dir_str) {
                        data_dir = std::filesystem::path(explicit_data_dir_str);
                } else {
                        data_dir = Util::find_data_dir();
                }

                Session session(data_dir);

                /* Set up qt */
                QCoreApplication::setOrganizationName("sispare");
                QCoreApplication::setApplicationName("sispare-qt");
                QApplication qa(argc, argv);
                UI ui(&session);

                ui.show();

                return qa.exec();
        } catch (const std::exception& ex) {
                Util::output_nested_ex(ex);
        }
}
