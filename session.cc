/*
 * Copyright (c) 2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "session.hh"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <random>
#include <regex>
#include <string>

std::string Session::salt = "will be overwritten";

/*
 * Start up a session attached to a directory like "~/.data/sispare".
 * Read in cards, set up state so that the UI can ask what's going on,
 * etc.
 */
Session::Session(const std::filesystem::path& data_dir) :

        data_dir(data_dir),
        session_start(std::chrono::system_clock::now()),
        current_action(Currently_doing::Trivial),
        cards(),
        card_status(),
        schedule_files_to_clean(),
        cards_to_delete()

{
        /*
         * Read all the cards in the schedule directory, figure out what
         * we need to review today.
         */
        const std::regex ymd_matcher("^(\\d+)-(\\d+)-(\\d+)$");
        const std::time_t now = std::chrono::system_clock::to_time_t(session_start);
        std::ostringstream gcc_still_doesnt_have_std_format;

        gcc_still_doesnt_have_std_format << now;
        Session::salt = gcc_still_doesnt_have_std_format.str();
        std::filesystem::create_directories(data_dir / "schedule");
        std::filesystem::create_directories(data_dir / "cards");

        for (const auto& entry : std::filesystem::directory_iterator(data_dir / "schedule")) {
                const std::filesystem::path& p = entry.path();

                /*
                 * p is something like
                 * "~/.data/sispare/schedule/2099-10-27. Is it that day
                 * yet?
                 */
                const std::string ymd = p.stem().string();
                std::smatch ymd_matches;
                struct tm this_tm = {};

                if (std::regex_search(ymd, ymd_matches, ymd_matcher)) {
                        this_tm.tm_year = std::stoi(ymd_matches[1]) - 1900;
                        this_tm.tm_mon = std::stoi(ymd_matches[2]) - 1;
                        this_tm.tm_mday = std::stoi(ymd_matches[3]);
                        const std::time_t sched_time = mktime(&this_tm);

                        if (sched_time < now) {
                                schedule_files_to_clean.push_back(p);
                                std::ifstream sched_is(p);
                                std::string line;

                                /*
                                 * The schedule file just contains a
                                 * number of lines, and
                                 * "~/.data/sispare/cards/<line>" is the
                                 * card to review.
                                 */
                                while (getline(sched_is, line)) {
                                        try {
                                                cards.insert(std::move(Card::mk(data_dir / "cards" / line)));
                                        } catch (const std::exception& ex) {
                                                cards_to_delete.insert(data_dir / "cards" / line);
                                        }
                                }
                        }
                }
        }

        if (!cards.empty()) {
                current_action = Currently_doing::Viewing_A;
                cards_it = cards.cbegin();
                it_index = 1;
        }
}

bool
Session::have_cards_to_review() const
{
        return !cards.empty();
}

Currently_doing
Session::get_current_action() const
{
        return current_action;
}

std::optional<const std::string>
Session::get_current_A_side() const
{
        if (cards.empty() ||
            cards_it == cards.end()) {
                return std::nullopt;
        }

        return cards_it->a;
}

std::optional<const std::string>
Session::get_current_B_side() const
{
        if (cards.empty() ||
            cards_it == cards.end()) {
                return std::nullopt;
        }

        return cards_it->b;
}

const std::string
Session::get_progress_string() const
{
        std::ostringstream out;

        out << it_index << "/" << cards.size();

        return out.str();
}

const std::string
Session::get_statistics() const
{
        std::ostringstream out;
        std::size_t num_passed = 0;
        std::size_t num_failed = 0;

        for (auto& c : cards) {
                auto ret = card_status.find(c.path);

                if (ret != card_status.end()) {
                        switch (ret->second) {
                        case Review_status::Pass_easy:
                        case Review_status::Pass_hard:
                                num_passed++;
                                break;
                        case Review_status::Fail:
                                num_failed++;
                                break;
                        case Review_status::Unreviewed:
                                break;
                        }
                }
        }

        out << "Finished." << std::endl << std::endl;
        out << "Passed: " << num_passed << std::endl;
        out << "Failed: " << num_failed << std::endl;

        return out.str();
}

const std::string
Session::get_previously_seen_string() const
{
        if (cards.empty() ||
            cards_it == cards.end()) {
                return "";
        }

        auto ret = card_status.find(cards_it->path);

        if (ret == card_status.end()) {
                return "";
        }

        switch (ret->second) {
        case Review_status::Pass_easy:
                return "Marked as passed";
        case Review_status::Pass_hard:
                return "Marked as passed (hard)";
        case Review_status::Fail:
                return "Marked as failed";
        case Review_status::Unreviewed:
                return "";
        }

        return "";
}

bool
Session::have_next_card() const
{
        return !cards.empty() &&
               cards_it != cards.end() &&
               std::next(cards_it) != cards.end();
}

bool
Session::have_prev_card() const
{
        return !cards.empty() &&
               cards_it != cards.begin();
}

void
Session::flip_current_card()
{
        switch (current_action) {
        case Currently_doing::Viewing_A:
                current_action = Currently_doing::Viewing_B;
                break;
        case Currently_doing::Viewing_B:
                current_action = Currently_doing::Viewing_A;
                break;
        default:
                break;
        }
}

void
Session::mark_current_card_as(Review_status review_status)
{
        if (cards.empty() ||
            cards_it == cards.end()) {
                return;
        }

        card_status[cards_it->path] = review_status;
        move_next_card();
}

void
Session::move_next_card()
{
        if (cards.empty()) {
                return;
        }

        if (have_next_card()) {
                current_action = Currently_doing::Viewing_A;
        } else {
                current_action = Currently_doing::Finished;
        }

        cards_it++;
        it_index++;
}

void
Session::move_prev_card()
{
        if (cards.empty()) {
                return;
        }

        if (have_prev_card()) {
                cards_it--;
                it_index--;
                current_action = Currently_doing::Viewing_A;
        }
}

void
Session::update_cards_on_disk()
{
        /*
         * First, go through all the cards and update their levels
         * according to whether they were passed or failed today. Write
         * out their new level (to the card's own directory) and the
         * next time they should be reviewed (to somewhere in the
         * schedule directory).
         */
        int cutoff_level = (std::end(timing_data) - std::begin(timing_data)) - 1;
        std::random_device rand_dev;
        std::mt19937 gen(rand_dev());

        for (auto& c : cards) {
                int new_level = c.level;
                auto ret = card_status.find(c.path);

                if (ret == card_status.end()) {
                        continue;
                }

                /* Calculate and write new level. */
                switch (ret->second) {
                case Review_status::Unreviewed:
                        break;
                case Review_status::Pass_easy:
                        new_level = std::min(new_level + 1, cutoff_level + 1);
                        break;
                case Review_status::Pass_hard:
                        new_level = std::max(new_level / 2, 1);
                        break;
                case Review_status::Fail:
                        new_level = 1;
                        break;
                }

                std::ofstream out_level(c.path / "level");

                out_level << new_level << std::endl;

                if (new_level > cutoff_level) {
                        /* Don't reschedule this card */
                        continue;
                }

                /* Choose when it'll get reviewed. Note uniform_int_distribution is inclusive of endpoints. */
                std::pair<int, int> delay_range = timing_data[new_level];
                std::uniform_int_distribution<> possible_waits(delay_range.first, delay_range.second);
                int days_to_wait = possible_waits(gen);
                std::time_t then = std::chrono::system_clock::to_time_t(session_start + std::chrono::days(days_to_wait));
                struct tm then_tm = *localtime(&then);

                /* Write to the proper schedule file */
                std::ostringstream date;

                date << (then_tm.tm_year + 1900);
                date << "-";
                date << std::setfill('0') << std::setw(2) << (then_tm.tm_mon + 1);
                date << "-";
                date << std::setfill('0') << std::setw(2) << then_tm.tm_mday;
                const std::string sched_day = date.str();
                std::ofstream out_sched_day(data_dir / "schedule" / sched_day, std::ios_base::app);

                out_sched_day << c.path.stem().string() << std::endl;
        }

        /*
         * Next, go through all the schedule files that we read from on
         * startup. Every line in those files that corresponds to a card
         * we reviewed needs to be wiped. If we quit the session early,
         * there might be some lines left over. Schedule files that end
         * up completely empty should just be deleted from disk.
         */
        for (auto& f : schedule_files_to_clean) {
                std::ifstream ii(f);
                std::ostringstream filtered;

                if (!ii.is_open()) {
                        continue;
                }

                /*
                 * The filter should only keep the cards we didn't
                 * review in this session. The cards that we did were
                 * re-scheduled above.
                 *
                 * Additionally, cards that caused errors on session
                 * load should be erased.
                 */
                std::string line;

                while (std::getline(ii, line)) {
                        /* Don't output deleted cards */
                        auto ret1 = cards_to_delete.find(data_dir / "cards" / line);

                        if (ret1 != cards_to_delete.end()) {
                                continue;
                        }

                        /* Don't output cards that were passed/failed this session */
                        auto ret2 = card_status.find(data_dir / "cards" / line);

                        if (ret2 == card_status.end()) {
                                filtered << line << std::endl;
                                continue;
                        }

                        switch (ret2->second) {
                        case Review_status::Pass_easy:
                        case Review_status::Pass_hard:
                        case Review_status::Fail:
                                break;
                        case Review_status::Unreviewed:
                                filtered << line << std::endl;
                                break;
                        }
                }

                ii.close();
                std::string filtered_str = filtered.str();

                /*
                 * Only keep the schedule file around if it has some
                 * cards in it. Otherwise, get rid of it.
                 */
                if (filtered_str.size() > 0) {
                        std::ofstream oo(f);

                        oo << filtered.str();
                } else {
                        std::filesystem::remove(f);
                }
        }
}
